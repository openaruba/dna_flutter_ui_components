import 'package:dna_flutter_ui_components/Components/MessageDisplay/MessageDisplayView.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../Components/LoadingAnimation/LoadingAnimationView.dart';
import 'ContentEmptyErrorLayoutModel.dart';

export 'ContentEmptyErrorLayoutModel.dart';

enum ContentEmptyErrorLoadingLayoutStateShowing {
  loading,
  content,
  message
}

class ContentEmptyErrorLoadingLayoutState {
  final String title;

  ContentEmptyErrorLoadingLayoutStateShowing showing = ContentEmptyErrorLoadingLayoutStateShowing.loading;
  ContentEmptyErrorLoadingLayoutState(this.title);
}

class ContentEmptyErrorLoadingLayout extends StatelessWidget {

  final Widget? stickyHeader;
  final Widget content;

  ContentEmptyErrorLoadingLayoutModel layoutModel;

  ContentEmptyErrorLoadingLayout(ContentEmptyErrorLoadingLayoutModel layoutModel, Widget? stickyHeader, Widget content):
        this.layoutModel = layoutModel,
        this.stickyHeader = stickyHeader,
        this.content = content;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        initialData: layoutModel.state,
        stream: layoutModel.stateStream,
        builder: (BuildContext context, AsyncSnapshot<ContentEmptyErrorLoadingLayoutState> snapshot) {
          return Scaffold(
            appBar: AppBar(
              // Here we take the value from the MyHomePage object that was created by
              // the App.build method, and use it to set our appbar title.
              title: Text(this.layoutModel.state.title),
            ),
            body: Center(
              child: Column(
                children: <Widget>[
                  this.stickyHeader ?? SizedBox.shrink(),
                  Expanded(
                    child: _loadingOrContent(),
                  ),
                ],
              ),
            ));
    });
  }

  Widget _loadingOrContent() {
    if(this.layoutModel.state.showing == ContentEmptyErrorLoadingLayoutStateShowing.loading) {
      return Container(
          child: Center(child: LoadingAnimationView())
      );
    }
    else if (this.layoutModel.state.showing == ContentEmptyErrorLoadingLayoutStateShowing.content) {
      return this.content;
    }
    else if (this.layoutModel.state.showing == ContentEmptyErrorLoadingLayoutStateShowing.message) {
        return MessageDisplayView(this.layoutModel.errorViewModel);
    }
    else {
      return SizedBox.shrink();
    }
  }
}

