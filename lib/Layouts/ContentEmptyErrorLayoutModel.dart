import 'package:dna_flutter_ui_components/Components/MessageDisplay/MessageDisplayView.dart';

import '../Components/ViewModel.dart';
import 'ContentEmptyErrorLoadingLayout.dart';

class ContentEmptyErrorLoadingLayoutModel extends ViewModel<ContentEmptyErrorLoadingLayoutState> {

  final MessageDisplayViewModel errorViewModel = MessageDisplayViewModel(MessageDisplayViewState());

  ContentEmptyErrorLoadingLayoutModel(state) : super(state);

  void showContent() {
    state.showing = ContentEmptyErrorLoadingLayoutStateShowing.content;
    notifyListeners();
  }

  void showMessage(String primaryMessage, String secondaryMessage, bool action, String actionText) {
    state.showing = ContentEmptyErrorLoadingLayoutStateShowing.message;
    errorViewModel.showMessage(primaryMessage, secondaryMessage, action, actionText);
    notifyListeners();
  }

  void showLoading() {
    state.showing = ContentEmptyErrorLoadingLayoutStateShowing.loading;
    notifyListeners();
  }
}