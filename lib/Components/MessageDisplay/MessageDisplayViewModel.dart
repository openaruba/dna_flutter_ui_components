library dna_flutter_ui_components;

import 'dart:async';
import 'package:dna_flutter_ui_components/Components/MessageDisplay/MessageDisplayView.dart';
import 'package:dna_flutter_ui_components/Components/ViewModel.dart';

import 'MessageDisplayView.dart';

export 'MessageDisplayViewModel.dart';

class MessageDisplayViewModel extends ViewModel<MessageDisplayViewState> {
    final retryTapped = StreamController<void>();

    // MARK: State

    MessageDisplayViewModel(state) : super(state);

    void clearError() {
        state.primaryMessage = "";
        state.secondaryMessage = "";
        state.hasAction = false;
        state.action = "";
        notifyListeners();
    }

    void showMessage(String primaryMessage, String secondaryMessage, bool hasAction, String action) {
        state.primaryMessage = primaryMessage;
        state.secondaryMessage = secondaryMessage;
        state.hasAction = hasAction;
        state.action = action;
        notifyListeners();
    }
}