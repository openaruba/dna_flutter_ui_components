library dna_flutter_ui_components;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'MessageDisplayViewModel.dart';

export 'MessageDisplayViewModel.dart';

class MessageDisplayViewState {
  String primaryMessage ="";
  String secondaryMessage = "";
  String action = "";
  bool hasAction = false;
}

class MessageDisplayView extends StatelessWidget {

  MessageDisplayViewModel viewModel;

  MessageDisplayView(MessageDisplayViewModel viewModel):
        this.viewModel = viewModel;

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(viewModel.state.primaryMessage, style: Theme.of(context).textTheme.headline5, textAlign: TextAlign.center),
              Text(viewModel.state.secondaryMessage, style: Theme.of(context).textTheme.headline6, textAlign: TextAlign.center),
              Visibility(
                  visible: this.viewModel.state.hasAction,
                  child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    TextButton(onPressed: ()=> { viewModel.retryTapped.add(null)}, child: Text(this.viewModel.state.action, style: Theme.of(context).textTheme.headline4))
                  ],
              ))
            ]
        ));
  }
}
