library dna_flutter_ui_components;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

export 'LoadingAnimationView.dart';

class LoadingAnimationView extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return CircularProgressIndicator();
  }
}
