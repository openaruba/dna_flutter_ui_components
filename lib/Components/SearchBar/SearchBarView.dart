library dna_flutter_ui_components;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'SearchBarViewModel.dart';

export 'SearchBarViewModel.dart';

class SearchBarViewState extends ChangeNotifier {
  final String _formKey;
  String get formKey => _formKey;

  final String _hintText;
  String get hintText => _hintText;

  final TextInputType _inputType;
  TextInputType get inputType => _inputType;

  SearchBarViewState(String formKey, String hintText, TextInputType inputType):
        this._formKey = formKey,
        this._hintText = hintText,
        this._inputType = inputType;
}

class SearchBarView extends StatelessWidget {

  SearchBarViewModel viewModel;

  SearchBarView(SearchBarViewModel viewModel):
        this.viewModel = viewModel;

  @override
  Widget build(BuildContext context) {
     return Container(
        child: Form(
          key: Key(viewModel.state.formKey),
          child: Row(children: [
            Expanded(child:
              TextFormField(
                onChanged: (text) => { this.viewModel.value.add(text) },
                onFieldSubmitted: (text) => { this.viewModel.submit.add(text) },
                decoration: InputDecoration(
                  hintText: viewModel.state.hintText,
                  contentPadding: EdgeInsets.all(20.0),
                ),
                textInputAction: TextInputAction.go,
                keyboardType: viewModel.state.inputType,
              ),
            )]
    )));
  }
}
