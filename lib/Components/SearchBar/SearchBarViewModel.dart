library dna_flutter_ui_components;

import 'dart:async';
import 'package:dna_flutter_ui_components/Components/ViewModel.dart';
import 'package:flutter/cupertino.dart';
import 'SearchBarView.dart';

class SearchBarViewModel extends ViewModel<SearchBarViewState> {

  // MARK: Output

  final value = StreamController<String>();
  final submit = StreamController<String>();

  SearchBarViewModel(SearchBarViewState initialState)
      : super(initialState);
}