
import 'package:flutter/cupertino.dart';
import 'package:rxdart/rxdart.dart';

class ViewModel<State> extends ChangeNotifier {

  State state;

  // Output
  Stream<State> get stateStream { return _stateSubject.stream; }

  PublishSubject<State> _stateSubject = PublishSubject<State>();

  ViewModel(State state): this.state = state {
    addListener(() {
      _stateSubject.add(this.state);
    });
  }
}