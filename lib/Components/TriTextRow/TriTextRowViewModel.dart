import 'dart:async';

import 'package:dna_flutter_ui_components/Components/ViewModel.dart';
import 'TriTextRowView.dart';

class TriTextRowViewModel extends ViewModel<TriTextRowViewState> {

  final tapped = StreamController<void>();

  TriTextRowViewModel(TriTextRowViewState state) : super(state);
}