library dna_flutter_ui_components;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'TriTextRowViewModel.dart';

export 'TriTextRowViewModel.dart';

class TriTextRowViewState {
  final String primaryText;
  final String subtitle1;
  final String subtitle2;

  TriTextRowViewState(String primaryText, String subtitle1, String subtitle2):
        this.primaryText = primaryText,
        this.subtitle1 = subtitle1,
        this.subtitle2 = subtitle2;
}

class TriTextRowView extends StatelessWidget {

  TriTextRowViewModel viewModel;

  TriTextRowView(TriTextRowViewModel viewModel):
        this.viewModel = viewModel;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: viewModel.stateStream,
        initialData: viewModel.state,
        builder: (BuildContext context, AsyncSnapshot<TriTextRowViewState> snapshot) {
          return TextButton(
              style: TextButton.styleFrom(
                  backgroundColor: Color.fromARGB(1, 1, 1, 1)
              ),
              onPressed: ()=> { viewModel.tapped.add(null)},

              child: Container(
                  child: Row(
                    children: [
                      Expanded(child:
                        Text(viewModel.state.primaryText , style: Theme.of(context).textTheme.headline6, overflow: TextOverflow.ellipsis)
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Text(viewModel.state.subtitle1, style: Theme.of(context).textTheme.subtitle1, textAlign: TextAlign.right),
                            Text(viewModel.state.subtitle2, style: Theme.of(context).textTheme.subtitle2, textAlign: TextAlign.right)
                          ],
                      )
                    ]
                ))
          );
    });
  }
}
