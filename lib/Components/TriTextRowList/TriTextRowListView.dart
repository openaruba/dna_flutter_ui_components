library dna_flutter_ui_components;

import 'package:dna_flutter_ui_components/Components/TriTextRow/TriTextRowView.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'TriTextRowListViewModel.dart';

export 'TriTextRowListViewModel.dart';

class TriTextRowListViewState {
  List<TriTextRowViewState> rowStates = [];
}

class TriTextRowListView extends StatelessWidget {

  TriTextRowListViewModel viewModel;

  TriTextRowListView(TriTextRowListViewModel viewModel):
        this.viewModel = viewModel;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(stream: viewModel.stateStream, initialData: viewModel.state, builder: (BuildContext context, AsyncSnapshot<TriTextRowListViewState> snapshot) {
      return ListView.builder(itemCount: snapshot.data?.rowStates.length ?? 0,
        itemBuilder: (BuildContext context, int index) {
          return TriTextRowView(this.viewModel.triTextViewModel(index));
        },
      );
    });
  }
}


