
import 'dart:async';
import 'package:dna_flutter_ui_components/Components/ViewModel.dart';
import '../TriTextRow/TriTextRowView.dart';
import 'TriTextRowListView.dart';

class TriTextRowListViewModel extends ViewModel<TriTextRowListViewState> {

  TriTextRowListViewModel(state) : super(state);

  void rows(List<TriTextRowViewState> rowStates) {
     state.rowStates = rowStates;
     notifyListeners();
  }

  void clear() {
    state.rowStates = [];
    notifyListeners();
  }

  // MARK: Output

  Stream<int> get indexTapped { return _indexTapped.stream; }
  final StreamController<int> _indexTapped = StreamController();

  int get rowsCount {
    return state.rowStates.length;
  }

  TriTextRowViewModel triTextViewModel(int index) {
    TriTextRowViewModel viewModel = TriTextRowViewModel(state.rowStates[index]);

    viewModel.tapped.stream.listen((event) { _indexTapped.add(index); });
    return viewModel;
  }
}