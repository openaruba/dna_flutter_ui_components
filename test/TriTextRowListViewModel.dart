import 'package:dna_flutter_ui_components/Components/TriTextRow/TriTextRowView.dart';
import 'package:dna_flutter_ui_components/Components/TriTextRowList/TriTextRowListView.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('TriTextRowListViewModel.clear()', () {
    final viewModel = TriTextRowListViewModel(TriTextRowListViewState());
    viewModel.clear();
    assert(viewModel.state.rowStates.isEmpty, "clear() must return empty rows");
  });

  test('TriTextRowListViewModel.rows(...)', () {
    final viewModel = TriTextRowListViewModel(TriTextRowListViewState());

    viewModel.rows([
      TriTextRowViewState("1", "2", "3"),
      TriTextRowViewState("1", "2", "3"),
      TriTextRowViewState("1", "2", "3")
    ]);

    assert(viewModel.state.rowStates.length == 3, "Rows were not set correctly.");
  });
}