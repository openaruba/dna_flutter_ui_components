import 'package:dna_flutter_ui_components/Components/MessageDisplay/MessageDisplayView.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('MessageDisplayViewModel.clearError()', () {
    final viewModel = MessageDisplayViewModel(MessageDisplayViewState());

    viewModel.clearError();
    assert(viewModel.state.primaryMessage == "");
    assert(viewModel.state.secondaryMessage == "");
    assert(viewModel.state.hasAction == false);
    assert(viewModel.state.action == "");
  });

  test('MessageDisplayViewModel.error(...)', () {
    final viewModel = MessageDisplayViewModel(MessageDisplayViewState());

    viewModel.showMessage("This is an error.", "secondary", false, "retry");
    assert(viewModel.state.primaryMessage == "This is an error.");
    assert(viewModel.state.secondaryMessage == "secondary");
    assert(viewModel.state.hasAction == false);
    assert(viewModel.state.action == "retr");
  });
}